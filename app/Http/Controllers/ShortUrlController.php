<?php

namespace App\Http\Controllers;

use App\Models\ShortUrl;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ShortUrlController extends Controller {

    /**
     * Função responsável por retornar um código aleatório de tamanho "n"
     *
     * @param int $length
     * @return string
     */
    private function randString($length) {

        $char = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $char = str_shuffle($char);

        for ($i = 0, $rand = '', $l = strlen($char) - 1; $i < $length; $i ++) {
            $rand .= $char[mt_rand(0, $l)];
        }

        return $rand;
    }

    /**
     * Encurtar URL
     *
     * @param Request $request
     * @return response
     */
    public function short(Request $request) {

        $validator = Validator::make($request->all(), [
            "url" => "string|url",
        ]);

        if ($validator->fails()) {
            return response($validator->errors()->first(), Response::HTTP_BAD_REQUEST);

        } else {

            try {

                // Salvar no banco de dados
                $shortUrl = new ShortUrl();
                $shortUrl->code = $this->randString(5);
                $shortUrl->url = $request->url;
                $shortUrl->created_at = now();
                $shortUrl->save();

                // Retornar URL de redirecionamento
                return url($shortUrl->code);

            } catch (\Exception $e) {
                return response($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
            }
        }
    }

    /**
     * Redirecionar para URL original
     *
     * @param Request $request
     * @param string $code
     * @return void
     */
    public function redirect(Request $request, $code) {

        $shortUrl = ShortUrl::where("code", $code)->first();

        if ($shortUrl) {
            return Redirect::to($shortUrl->url);

        } else {
            abort(404);
        }
    }
}
