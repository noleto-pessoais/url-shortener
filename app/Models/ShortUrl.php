<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Short URL
 *
 * @author Victor Noleto <noletovasco@gmail.com>
 * @since 03/08/2021
 * @version 1.0.0
 */
class ShortUrl extends Model {

    protected $table = "short_urls";

    public $timestamps = false;

}
