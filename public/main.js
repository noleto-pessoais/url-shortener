"use strict";

$(function() {

    var $form = $("form");
    var $url = $form.find(`[name="url"]`);
    var $shortUrl = $form.find(`[name="short_url"]`);
    var $btnCopy = $form.find(".btn-copy-to-clipboard");
    var $btnTest = $form.find(".btn-test");

    function shortUrl() {

        var data = {
            url: $url.val(),
            _token: $form.find(`[name="_token"]`).val(),
        };

        var successCallback = function(shortUrl) {

            var $group = $shortUrl.closest(".form-group");
            var $originalLink = $group.find(".label-wrapper small a");

            $url.val("");
            $shortUrl.val(shortUrl);

            $originalLink.attr("href", data.url);
            $originalLink.text(data.url);

            $group.removeClass("d-none");
            $btnCopy.trigger("focus");
        };

        var errorCallback = function(error) {

            console.error(error);

            bootbox.dialog({
                title: "Atenção!",
                message: `<p>Não foi possível encurtar a URL: <b>${ error.responseText }</b></p>`,
                closeButton: false,
                buttons: {
                    retry: {
                        label: "Tentar novamente",
                        className: "btn-light",
                        callback: shortUrl
                    },
                    ok: {
                        label: "Ok",
                        className: "btn-primary"
                    }
                }
            });
        };

        $.ajax({
            method: $form.attr("method"),
            url: $form.attr("action"),
            data: data,
            success: successCallback,
            error: errorCallback
        });
    };

    function message($input, type, message) {

        var $wrapper = $input.closest(".form-group").find(".input-messages");
        var $p = $(`<p class="animate__animated animate__fadeIn ${ 'text-' + type }">${ message }</p>`);
        $wrapper.append($p);

        setTimeout(function() {

            $p.removeClass("animate__fadeIn").addClass("animate__fadeOut");

            // Esperar fadeOut para remover o elemento
            setTimeout(function() {
                $p.remove();
            }, 2000);

        }, 3000);
    }

    $form.on("submit", function(e) {
        e.preventDefault(); shortUrl(); return false;
    });

    $btnCopy.on("click", function() {

        if ($shortUrl.val() != "") {

            copyTextToClipboard($shortUrl.val()).then(function(success) {
                message($shortUrl, "success", `A URL foi copiada para a área de transferência com sucesso!`);

            }).catch(function(err) {
                message($shortUrl, "danger", `Não foi possível copiar o texto: <b>${ err }</b>`);
            });

        } else {
            return false;
        }

    });

    $btnTest.on("click", function() {

        if ($shortUrl.val() != "") {
            window.open($shortUrl.val(), '_blank');

        } else {
            return false;
        }

    });

    /* $url.val('http://www.google.com');
    shortUrl(); */

});

/**
 * Caso o navegador não tenha suporte para a cópia otimizada, executar esse script
 * @param {*} text
 * @returns
 */
function fallbackCopyTextToClipboard(text) {

	return new Promise(function(resolve, reject) {

		var textArea = document.createElement("textarea");
		textArea.value = text;

		// Avoid scrolling to bottom
		textArea.style.top = "0";
		textArea.style.left = "0";
		textArea.style.position = "fixed";

		document.body.appendChild(textArea);
		textArea.focus();
		textArea.select();

		try {

			var successful = document.execCommand('copy');

			if (successful) {
				resolve(text);

			} else {
				resolve('Fallback: Copying text command was unsuccessful');
			}

		} catch (err) {
			reject(err);
		}

		document.body.removeChild(textArea);

	});
}

/**
 * Copiar texto para clipboard
 * @param {*} text
 * @returns
 */
function copyTextToClipboard(text) {

	if (navigator.clipboard) {
		return navigator.clipboard.writeText(text);

	} else {
		return fallbackCopyTextToClipboard(text);
	}
}
