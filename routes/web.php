<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

// Encurtar URL
Route::post('short-url', 'ShortUrlController@short');

// Redirecionar através do código
Route::get('/{code}', 'ShortUrlController@redirect');
