<html lang="en">
<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Google fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@300;400;500;700&display=swap" rel="stylesheet">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    {{-- animate.css --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <link href="{{ asset('main.css') }}" rel="stylesheet">

    <title>{{ env('APP_NAME') }}</title>

</head>

<body>

    <div class="container">

        <div class="title">
            <h1>Simplifique os seus links</h1>
            <p>Victor Noleto - noletovasco@gmail.com</p>
        </div>

        <form method="POST" action="{{ url('short-url') }}">

            @csrf

            <div class="form-group mb-4">
                <label>Informe uma URL:</label>
                <div class="input-wrapper">
                    <input type="text" name="url" class="form-control" required />
                    <button type="submit" class="btn btn-primary" title="Submeter">Encurtar URL</button>
                </div>
                <div class="input-messages"></div>
            </div>

            <div class="form-group mb-0 d-none">
                <div class="label-wrapper">
                    <label>URL encurtada: </label>
                    <small>URL original: <b><a href="" target="_blank"></a></b></small>
                </div>
                <div class="input-wrapper">
                    <input type="text" name="short_url" class="form-control" readonly />
                    <button type="button" class="btn btn-secondary btn-copy-to-clipboard" title="Copiar para a área de transferência">Copiar</button>
                    <button type="button" class="btn btn-dark btn-test" title="Ir para esta URL">Testar</button>
                </div>
                <div class="input-messages"></div>
            </div>

        </form>

    </div>

    <!-- jQuery JS -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <!-- Bootstrap JS -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>

    {{-- Bootbox JS --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.min.js"></script>

    <script src="{{ asset('main.js') }}"></script>

</body>
</html>
